﻿using Coinigy_v2_api_2018;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Http;

namespace Coinigy_v2_api_test
{
    class Program
    {
        static void Main(string[] args)
        {
            ApiRequest req = new ApiRequest();
            req.BaseUrl = "https://api.coinigy.com";
            req.Body = "";
            req.Method = "GET";
            req.Secret = "secret";
            req.Key = "key";
            req.Endpoint = "/api/v2/private/exchanges";
            
            string signature = req.Signature;
            HttpResponseMessage response = null;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("X-API-SIGN", signature);
                client.DefaultRequestHeaders.Add("X-API-TIMESTAMP", req.Timestamp);
                client.DefaultRequestHeaders.Add("X-API-KEY", req.Key);
                response = client.GetAsync(req.BaseUrl + req.Endpoint).Result;
            }
            string r = null;
            if (response.IsSuccessStatusCode)
            {
                r = response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                Console.Write(response);
                Console.WriteLine("");
                Console.WriteLine("X-API-KEY: " + req.Key);
                Console.WriteLine("X-API-SIGN: " + signature);
                Console.WriteLine("X-API-TIMESTAMP: " + req.Timestamp);
                Console.WriteLine("");
                Console.WriteLine("Whole url: " + req.BaseUrl + req.Endpoint);
                Console.WriteLine("Endpoint: " + req.Endpoint);
                Console.WriteLine("Method: " + req.Method);
                Console.WriteLine("Body: " + req.Body);
                Console.WriteLine("Secret: " + req.Secret);

            }

            Console.WriteLine(r);
            Console.ReadLine();


        }
    }
}
